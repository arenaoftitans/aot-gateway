dev:
	pipenv run uvicorn app.main:app --reload

lint:
	pipenv run flake8 app
	pipenv run black app --line-length 100 --check

dockerimage:
ifdef VERSION
	docker build \
	    -f docker/Dockerfile \
	    -t "registry.gitlab.com/arenaoftitans/aot-gateway:${VERSION}" \
	    .
	# Testing image
	docker run \
		-d \
		--rm \
		--name aot-aot-gateway-test \
		"registry.gitlab.com/arenaoftitans/aot-gateway:${VERSION}"
	docker rm -f aot-aot-gateway-test
	docker push "registry.gitlab.com/arenaoftitans/aot-gateway:${VERSION}"
else
	@echo "You must supply VERSION"
	exit 1
endif
