Game gateway
############

It is used a proxy to forward messages from nginx (which is directly facing the web) and the game API which are on a private docker network. For this service to work, you need to expose its port (configurable with the ``PORT`` env variable) with docker.

Configuration of the service is done with the following environment variables:

.. code::

    PORT  # Port on which the service must listen
    REDIS # HOST:PORT of the redis to connect to to save the API URL.
    LOG_LEVEL
    GATEWAY_SECRET # Secret to pass to allow registration.
    API_HOST # Default API host (used to forward messages)
    API_PORT # API port (used to forward messages)
    API_PROTOCOL # API protocol (used to forward messages). Can be either ws or wss


Usage
=====

- To register a new version of latest, do ``http POST localhost:8180/register/latest <<<'{"name": "localhost"}'``.
- To view the latest version, do ``http localhost:8180/register/latest``.
- To forward messages, connect to ``localhost:8180/ws/API_VERSION``.


Requirements
============

- `pipenv <https://github.com/pypa/pipenv#installation>`__ and Python 3.9+.


Update deps
===========

#. ``pipenv update --dev``
#. ``pipenv sync --dev``
#. ``make lint``
#. Check we can run the result with ``uvicorn app.main:app``.
#. ``make VERSION=VERSION dockerimage`` We now use calendar versioning here (we used semver before).
#. Update ``docker-compose.yaml`` in API.


Development
===========

#. Install dependencies with ``pipenv install --dev``
#. Run the project with ``make dev``
