import logging
from typing import List

from pydantic import IPvAnyAddress, RedisDsn
from pydantic_settings import BaseSettings


class Settings(BaseSettings):
    port: int = 8180
    redis_url: RedisDsn = "redis://localhost:6379/0"
    log_level: int = logging.INFO
    allowed_hosts: List[str] = []
    allowed_ips: List[IPvAnyAddress] = []
    gateway_secret: str = "change_this"
    api_host: str = "localhost"
    api_port: int = 8181
    api_protocol: str = "ws"


settings = Settings()
