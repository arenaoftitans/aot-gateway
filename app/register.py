import logging
from contextlib import contextmanager

from coredis import Redis as StrictRedis

from .settings import settings

REDIS_KEY = "gateway-api-latest"
DEFAULT_API_VERSION = "latest"
logger = logging.getLogger(__name__)


class RegistrationError(Exception):
    pass


async def register_latest_api_version(api_name, supplied_secret):
    if not supplied_secret or not settings.gateway_secret:
        raise RegistrationError(
            "API secret is not defined in the message or in the configuration. "
            "Cannot update registration."
        )
    if supplied_secret != settings.gateway_secret:
        raise RegistrationError("Supplied secret is invalid. Cannot update registration.")

    await _save_in_redis(api_name)


async def _save_in_redis(api_version: str):
    with _get_redis() as client:
        await client.set(REDIS_KEY, api_version)

    logger.info("Save %s as latest version in Redis" % api_version)


async def fetch_latest_api_version():
    with _get_redis() as client:
        api_version = (await client.get(REDIS_KEY)) or DEFAULT_API_VERSION

    logger.info("Retrieved %s as latest API connection from Redis" % api_version)
    return api_version


@contextmanager
def _get_redis():
    client = StrictRedis(host=settings.redis_url.host, port=settings.redis_url.port)
    yield client
    # The client doesn't need to be explicitly closed.
