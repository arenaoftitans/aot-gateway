import logging

from fastapi import FastAPI
from pydantic import BaseModel, validator

from .register import RegistrationError, fetch_latest_api_version, register_latest_api_version
from .settings import settings
from .ws import router as ws_router

app = FastAPI()
app.include_router(ws_router)

logging.basicConfig(level=settings.log_level)
logger = logging.getLogger(__name__)
logger.info(f"Starting gateway with on port {settings.port}")


class RegistrationMessage(BaseModel):
    name: str
    secret: str

    @validator("name")
    def prevent_empty_name(cls, value):  # noqa: N805 first argument should be named 'self'
        if not value:
            raise ValueError("Empty strings are not allowed")
        return value

    @validator("secret")
    def prevent_empty_secret(cls, value):  # noqa: N805 first argument should be named 'self'
        if not value:
            raise ValueError("Empty strings are not allowed")
        return value


@app.get("/")
def status():
    return {"working": True}


@app.post("/register/latest")
async def register_latest(message: RegistrationMessage):
    try:
        await register_latest_api_version(message.name, message.secret)
    except RegistrationError as e:
        return {"success": False, "errors": [str(e)]}
    else:
        return {"success": True, "errors": []}


@app.get("/register/latest")
async def fetch_latest():
    version = await fetch_latest_api_version()
    return {"name": version}
