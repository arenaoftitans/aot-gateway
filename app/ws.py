import asyncio
import logging

import websockets
from fastapi import APIRouter, WebSocket

from .register import DEFAULT_API_VERSION, fetch_latest_api_version
from .settings import settings

router = APIRouter()

logger = logging.getLogger(__name__)


@router.websocket("/ws/{api_version}")
async def ws_proxy(api_version: str, client_ws: WebSocket):
    await client_ws.accept()

    # If the supplied version is latest, we need to convert it to the proper version.
    if api_version == DEFAULT_API_VERSION:
        api_version = await fetch_latest_api_version()

    api_url = _build_api_url(api_version)
    logging.info(f"Connecting to {api_url}")
    async with websockets.connect(api_url) as api_ws:
        proxy_to_api = asyncio.create_task(_create_proxy_to_api(client_ws, api_ws))
        proxy_to_client = asyncio.create_task(_create_proxy_to_client(client_ws, api_ws))
        await asyncio.wait({proxy_to_api, proxy_to_client}, return_when=asyncio.FIRST_COMPLETED)
    return {"version": api_version}


async def _create_proxy_to_api(client_ws: WebSocket, api_ws):
    try:
        while True:
            msg = await client_ws.receive_text()
            logger.debug("Forwarding %s to API" % msg)
            await api_ws.send(msg)
    except Exception:
        logger.exception("Connection was closed.")
    finally:
        await _cleanup(client_ws, api_ws)


async def _create_proxy_to_client(client_ws: WebSocket, api_ws):
    try:
        while True:
            msg = await api_ws.recv()
            logger.debug("Forwarding %s to client" % msg)
            await client_ws.send_text(msg)
    except Exception:
        logger.exception("Connection was closed")
    finally:
        await _cleanup(client_ws, api_ws)


async def _cleanup(client_ws: WebSocket, api_ws):
    try:
        await client_ws.close()
    except Exception:
        logger.error("Failed to close client ws properly.")


def _build_api_url(api_version: str):
    host = settings.api_host
    if not host:
        host = api_version

    if isinstance(host, bytes):
        host = host.decode("utf-8")

    return f"{settings.api_protocol}://{host}:{settings.api_port}/ws/latest"
